## 0.6.0 (2024-03-19)

### BREAKING CHANGE

- 🚨 revamp api methods

### Feat

- ✨ revamp api methods

## 0.5.0 (2024-03-19)

### BREAKING CHANGE

- pydantic 2.x is now required

### Feat

- ✨ migrate to pydantic 2.x

## v0.3.3 (2021-04-19)

## v0.3.2 (2021-04-19)

## v0.3.1 (2021-04-19)

## v0.2.1 (2021-04-18)

## v0.2.0 (2021-04-18)

## v0.1.8 (2021-04-18)

## v0.1.4 (2021-04-18)

## v0.1.2 (2021-04-18)

## v0.1.0 (2021-04-17)

## v0.0.1 (2021-04-17)

## v0.0.0 (2021-04-16)
